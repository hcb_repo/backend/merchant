package com.sprintsquads.merchant.controller;

import com.sprintsquads.merchant.model.Merchant;
import com.sprintsquads.merchant.service.MerchantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/merchant")
@RequiredArgsConstructor
public class MerchantController {

    private final MerchantService merchantService;


    @PostMapping
    public String createMerchant(@RequestBody Merchant merchant){
        return merchantService.createMerchant(merchant).getId().toString();
    }

    @GetMapping(value = "/{id}")
    public Merchant getMerchant(@PathVariable Long id){
        return merchantService.get(id);
    }

    @PutMapping
    public ResponseEntity<Merchant> updateMerchant(@RequestBody Merchant merchant){
        return ResponseEntity.ok(merchantService.update(merchant));
    }
}
