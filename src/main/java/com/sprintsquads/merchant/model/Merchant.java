package com.sprintsquads.merchant.model;

import lombok.Data;
import org.hibernate.annotations.GenerationTime;


import javax.annotation.Generated;
import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "merchant")
public class Merchant {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;
    private String description;
    @Column(unique = true)
    private String pan;



}
