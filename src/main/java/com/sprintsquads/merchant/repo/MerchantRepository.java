package com.sprintsquads.merchant.repo;

import com.sprintsquads.merchant.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantRepository extends JpaRepository<Merchant, Long> {
}
