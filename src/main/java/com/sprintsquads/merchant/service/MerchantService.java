package com.sprintsquads.merchant.service;

import com.sprintsquads.merchant.model.Merchant;

public interface MerchantService {

    Merchant createMerchant(Merchant merchant);
    Merchant get(Long id);
    Merchant update(Merchant merchant);
}
