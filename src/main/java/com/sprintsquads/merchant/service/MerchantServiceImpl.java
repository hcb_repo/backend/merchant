package com.sprintsquads.merchant.service;

import com.sprintsquads.merchant.exception.EntityNotFoundException;
import com.sprintsquads.merchant.model.Merchant;
import com.sprintsquads.merchant.repo.MerchantRepository;
import dto.MerchantDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class MerchantServiceImpl implements MerchantService{

    private final MerchantRepository merchantRepository;

    @Override
    public Merchant createMerchant(Merchant merchant) {
        return merchantRepository.save(merchant);
    }

    @Override
    public Merchant get(Long id) {
        return merchantRepository.findById(id).orElse(null);
    }

    @Override
    public Merchant update(Merchant merchant) {
        if (merchant.getId() == null) throw new EntityNotFoundException();
        return merchantRepository.save(merchant);
    }
}
