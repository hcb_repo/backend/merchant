package dto;

import lombok.Data;

@Data
public class MerchantDto {
    private String id;
    private String name;
    private String description;
    private String iBan;
    private String checkOrderExistUrl;
}
